const fs = require('fs');
const https = require('https');
const stringSimilarity = require("string-similarity");
const csv = require('csv-parser')
const fsExtra = require('fs-extra')
require('dotenv').config()
createDirs()
createTestFiles()

function createDirs () {
  if (!fs.existsSync('./cypress/tests/completados')){
    fs.mkdirSync('./cypress/tests/completados');
  }
  if (!fs.existsSync('./cypress/tests/pendientes')){
    fs.mkdirSync('./cypress/tests/pendientes');
  }
  if (!fs.existsSync('./reports/screenshot')){
    fs.mkdirSync('./reports/screenshot', { recursive: true });
  }
  if (!fs.existsSync('/session/cookies.json')){
    fs.mkdirSync('./session', { recursive: true });
    fs.writeFile('./session/cookies.json','[ ]', () => {})

  }
  fsExtra.emptyDirSync('./cypress/downloads')
}

function createTestFiles(req, res) {
  const results = []
  fs.createReadStream('data/data.csv')
    .pipe(csv({
      mapHeaders: ({ header }) => '--' + header,
      mapValues: ({ header, index, value }) => value.trim().toUpperCase()
    }))
    .on('data', (data) => results.push(data))
    .on('end', async function() {
      const rowsToProcess = results.filter((i) => {
        const processar = i['--PROCESAR'] === 'TRUE'
        const procedimiento = i['--procedimiento']
        const hasProcedimiento = (procedimiento) && (procedimiento !== '') &&  (!procedimiento.toLowerCase().includes('xxxxx'))
        return processar && hasProcedimiento
      })
      console.log(' ')
      console.log('==== START ====')
      for (let i = 0; i < rowsToProcess.length; i++) {
          await processSolicitud(rowsToProcess[i], i, rowsToProcess.length)
      }
      console.log(' ')
      console.log('==== END ====')
      console.log(' ')
    })
}

async function processSolicitud(mappedObject, i, total) {
  console.log(' ')
  console.log('Procesando solicitud '  + (i + 1) + '/' + total + ':')
  await downloadFile(mappedObject['--document_download'], mappedObject['--soli_documento_identificativo'])
    .then(async function() {
      await writeTestFile(mappedObject)
        .then(() => {
          console.log(' - OK')
        })
        .catch( e => console.error('', e));
    })
    .catch(async function() {
      await writeTestFile(mappedObject)
        .then(() => {
          console.log(' - Atención: Se ha creado el test pero no se pudo descargar el fichero, añadir manualmente para: ' + mappedObject['--soli_documento_identificativo'])
        })
        .catch( e => console.error('', e));
    });
}

function replaceAll (str, mapObj) {
    var re = new RegExp(Object.keys(mapObj).join("|"),"gi")
    return str.replace(re, function(matched) {
      // IN CASE AUTONOMOS COLABORADORES CAME AS EMPTY
      if (matched === '--autonomos_colaboradores_numero' && mapObj[matched] === '') {
        return '0'
      }
      if (matched === '--minimis_recibida' && mapObj[matched] === '') {
        return '0'
      }
      if (matched === '--codigo_actividad_iae' && mapObj[matched] === '') {
        return '031'
      }
      // MATCHES PROVINCE
      if (matched === '--soli_provincia') {

        const province = mapObj[matched].normalize("NFD").replace(/[\u0300-\u036f]/g, "")
        const arrayOfProvinces = [{"value": "02", "text": "ALBACETE"}, {"value": "03", "text": "ALICANTE/ALACANT"}, {"value": "04", "text": "ALMERÍA"}, {"value": "01", "text": "ARABA/ÁLAVA"}, {"value": "33", "text": "ASTURIAS"}, {"value": "05", "text": "ÁVILA"}, {"value": "06", "text": "BADAJOZ"}, {"value": "07", "text": "BALEARS, ILLES"}, {"value": "08", "text": "BARCELONA"}, {"value": "48", "text": "BIZKAIA"}, {"value": "09", "text": "BURGOS"}, {"value": "10", "text": "CÁCERES"}, {"value": "11", "text": "CÁDIZ"}, {"value": "39", "text": "CANTABRIA"}, {"value": "12", "text": "CASTELLÓN/CASTELLÓ"}, {"value": "51", "text": "CEUTA"}, {"value": "13", "text": "CIUDAD REAL"}, {"value": "14", "text": "CÓRDOBA"}, {"value": "15", "text": "CORUÑA, A"}, {"value": "16", "text": "CUENCA"}, {"value": "20", "text": "GIPUZKOA"}, {"value": "17", "text": "GIRONA"}, {"value": "18", "text": "GRANADA"}, {"value": "19", "text": "GUADALAJARA"}, {"value": "21", "text": "HUELVA"}, {"value": "22", "text": "HUESCA"}, {"value": "23", "text": "JAÉN"}, {"value": "24", "text": "LEÓN"}, {"value": "25", "text": "LLEIDA"}, {"value": "27", "text": "LUGO"}, {"value": "28", "text": "MADRID"}, {"value": "29", "text": "MALAGA"}, {"value": "52", "text": "MELILLA"}, {"value": "30", "text": "MURCIA"}, {"value": "31", "text": "NAVARRA"}, {"value": "32", "text": "OURENSE"}, {"value": "34", "text": "PALENCIA"}, {"value": "35", "text": "PALMAS, LAS"}, {"value": "36", "text": "PONTEVEDRA"}, {"value": "26", "text": "RIOJA, LA"}, {"value": "37", "text": "SALAMANCA"}, {"value": "38", "text": "SANTA CRUZ DE TENERIFE"}, {"value": "40", "text": "SEGOVIA"}, {"value": "41", "text": "SEVILLA"}, {"value": "42", "text": "SORIA"}, {"value": "43", "text": "TARRAGONA"}, {"value": "44", "text": "TERUEL"}, {"value": "45", "text": "TOLEDO"}, {"value": "46", "text": "VALENCIA/VALÈNCIA"}, {"value": "47", "text": "VALLADOLID"}, {"value": "49", "text": "ZAMORA"}, {"value": "50", "text": "ZARAGOZA"}]

        const match = stringSimilarity.findBestMatch(
          province.toUpperCase(),
          arrayOfProvinces.map(i => i.text.toUpperCase())
        ).bestMatch;

        if (match.rating > 0.3) {
          return arrayOfProvinces.find(i => i.text === match.target).value
        } else {
          console.log(' - Error en el campo soli_provincia, tiene valor: ' + mapObj[matched] + ' pero no hay ningun campo similar para el solicitante: ' + mapObj['--soli_documento_identificativo'])
          console.log(' - Se asignará la provincia a: ALBACETE')
          return '02'
        }
      }
      return mapObj[matched]
    });
}

function writeTestFile (mapObj) {
  return new Promise((resolve, reject) => {

    try {
      if (fs.existsSync('./cypress/tests/completados/' + mapObj['--soli_documento_identificativo'] + '.js')) {
        reject('- NO SE COMPLETARÁ, LA SOLICITUD YA SE ENCUENTRA COMPLETADA');
      } else {
        if (process.env.NODE_ENV.includes('pro')) {
          if (process.env.AGENTE_DIGITALIZADOR.toLowerCase() == 'true' || process.env.AGENTE_DIGITALIZADOR == '1' ) {
            fs.readFile('./cypress/tests/base.js', 'utf8', function (err,data) {
              if (err) {
                reject(`we couldn't read the base.js test file`);
              }
              const result = replaceAll(data, mapObj)

              fs.writeFile('./cypress/tests/pendientes/' + mapObj['--soli_documento_identificativo'] + '.js', result, 'utf8', function (err) {
                 if (err) {
                   reject(`we couldn't write the ` +  mapObj['--soli_documento_identificativo'] + ' test file', err);
                 }
                 resolve();
               });
            });
          }
          else {
            fs.readFile('./cypress/tests/base-otro.js', 'utf8', function (err,data) {
              if (err) {
                reject(`we couldn't read the base.js test file`);
              }
              const result = replaceAll(data, mapObj)

              fs.writeFile('./cypress/tests/pendientes/' + mapObj['--soli_documento_identificativo'] + '.js', result, 'utf8', function (err) {
                 if (err) {
                   reject(`we couldn't write the ` +  mapObj['--soli_documento_identificativo'] + ' test file', err);
                 }
                 resolve();
               });
            });
          }
        }
        else {
          fs.readFile('./cypress/tests/base-pre.js', 'utf8', function (err,data) {
            if (err) {
              reject(`we couldn't read the base.js test file`);
            }
            const result = replaceAll(data, mapObj)

            fs.writeFile('./cypress/tests/pendientes/' + mapObj['--soli_documento_identificativo'] + '.js', result, 'utf8', function (err) {
               if (err) {
                 reject(`we couldn't write the ` +  mapObj['--soli_documento_identificativo'] + ' test file', err);
               }
               resolve();
             });
          });
        }
      }
    } catch(err) {
      reject(err)
    }
  })
}

function downloadFile(url, fileName) {
    return new Promise((resolve, reject) => {
        const dest = './cypress/downloads/' + fileName + '.pdf'
        const file = fs.createWriteStream(dest, { flags: "wx" });
        const request = https.get(url.toLowerCase(), response => {
            if (response.statusCode === 200) {
                response.pipe(file);
            } else {
                file.close();
                fs.unlink(dest, () => {}); // Delete temp file
                reject(`Server responded with ${response.statusCode}: ${response.statusMessage}`);
            }
        });

        request.on("error", err => {
            file.close();
            fs.unlink(dest, () => {}); // Delete temp file
            reject(err.message);
        });

        file.on("finish", () => {
            resolve();
        });

        file.on("error", err => {
            file.close();
            if (err.code === "EEXIST") {
                fs.unlink(dest, () => {}); // Delete temp file
                resolve();
            } else {
                fs.unlink(dest, () => {}); // Delete temp file
                reject(err.message);
            }
        });
    });
}
