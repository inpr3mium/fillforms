
# [Documentación de uso](https://docs.google.com/document/d/1U3IVPmHyyQOdTltfalGKCakwymNGBT_CChSVXe_di6Q/edit?usp=sharing)

# SETUP

---

 - Requisitos:
  - [Node](https://nodejs.org/es/download/)
  - [Certificado digital](https://www.sede.fnmt.gob.es/certificados/persona-fisica/obtener-certificado-software)
  - [AutoFirma](https://www.sede.fnmt.gob.es/certificados/persona-fisica/obtener-certificado-software)
  - Google Chrome

---


## Instalación:

```
npm install
```

### 2. AÑADIR ARCHIVO DE DATOS

 - El archivo CSV de datos (data.csv) debe encontrarse en la carpeta cypress/data


### 3. PRE-PROCESADO DE DATOS

```
npm run create
```

Este comando descargará los archivos necesarios y creará los tests en la carpeta de test/pendientes para poder ser ejecutados con Cypress y ser procesados automáticamente.

Si hubiera algún formulario que ya ha sido enviado a traves del portal quedará registro en tests/completados y este no se volverá a generar

## EJECUTAR EL ENTORNO DE CYPRESS Y EMPEZAR A RELLENAR FORMULARIOS

```
npm run start
```
