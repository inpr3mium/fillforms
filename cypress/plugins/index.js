/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars

// in plugins/index.js
const fs = require('fs')
require('dotenv').config()

module.exports = (on, config) => {
  config.env.URL = process.env.URL
  config.env.URL_PRE = process.env.URL_PRE
  on('task', {
    moveFile(file) {
      return new Promise((resolve, reject) => {
        fs.rename('cypress/tests/pendientes/' + file + '.js', 'cypress/tests/completados/' + file + '.js', function (err) {
          if (err) {
            if (err.code === 'EXDEV') {
              copy();
              resolve('file moved')
            }
          } else {
            resolve('file moved')
          }
          reject(err)
        });
        function copy() {
          var readStream = fs.createReadStream(oldPath);
          var writeStream = fs.createWriteStream(newPath);

          readStream.on('error', callback);
          writeStream.on('error', callback);

          readStream.on('close', function () {
              fs.unlink(oldPath, callback);
          });

          readStream.pipe(writeStream);
        }
      })
    },
  })
  return config
}
