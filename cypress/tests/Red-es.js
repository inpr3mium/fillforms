const DNI_PRUEBAS = '53290137B'

const soli_documento_identificativo = '53290137B'
const soli_razon_social = 'Arianna Russo'
const soli_telefono_movil = '609313584'
const soli_email = 'test@farmapremium.es'
const soli_persona_contacto = 'Arianna Russo'


// const RAZON_SOCIAL_AUTORIZADO = 'Carles Sebastian Martinez'

const representante_nif = '53290137B'
const nombre_notario = 'Nestor'
const apellido1_notario = 'Notario'
const apellido2_notario = 'del Alamo '
const protocolo_notario = '1'
const fecha_notario = '29/12/2010'

const entidad_autorizada_nif = 'B65758682'
const entidad_autorizada_razon_social = 'Inteligencia Del Negocio Pr3mium Sl'

const autonomos_colaboradores_numero = 1

const AC_1_autonomo_nif = '89652490P'
const AC_1_autonomo_nombre = 'Pepito'
const AC_1_autonomo_apellidos = 'Palote'

const AC_2_autonomo_nif = '89652490P'
const AC_2_autonomo_nombre = 'Pepito'
const AC_2_autonomo_apellidos = 'Palote'

const AC_3_autonomo_nif = '89652490P'
const AC_3_autonomo_nombre = 'Pepito'
const AC_3_autonomo_apellidos = 'Palote'

const AC_4_autonomo_nif = '89652490P'
const AC_4_autonomo_nombre = 'Pepito'
const AC_4_autonomo_apellidos = 'Palote'

const codigo_actividad_iae = '031'
const minimis_recibida = '0'

const getIframeDocument = () => {
  return cy.get('iframe').its('0.contentDocument').should('exist')
}
const getIframeBody = () => {
  // get the document
  return getIframeDocument()
  // automatically retries until body is loaded
  .its('body').should('not.be.undefined')
  // wraps "body" DOM element to allow
  // chaining more Cypress commands, like ".find(...)"
  .then(cy.wrap)
}

describe('Base test', () => {
  it('should open form', () => {

    cy.visit('https://sedepkd.pre.red.es/oficina/tramites/altaSolicitud.do?proc=C005')
    cy.xpath('/html/body/div[5]/div[4]/div/div[1]/form/fieldset[1]/div/div[2]/button/span[2]').click()


    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo_ss']").type(DNI_PRUEBAS, {force: true})
    cy.wait(200)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:nif_sgad']").type(DNI_PRUEBAS, {force: true})
    cy.wait(200)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo_aeat']").type(DNI_PRUEBAS, {force: true})
    cy.wait(200)


    getIframeBody().find("select").filter("[id='formRenderer:soli_empresa_autoempleo']").select('autoempleo') // Representante voluntario
    cy.wait(1000)
    getIframeBody().find("select").filter("[id='formRenderer:soli_provincia']").select('02') // Representante voluntario
    cy.wait(1000)
    getIframeBody().find("input[type='radio']").filter("[id='formRenderer:soli_empresas_vinculadas:1']").check() // Check checkbox element
    cy.wait(1000)

    getIframeBody().find("select").filter("[id='formRenderer:representante_tipo']").select('02') // Representante voluntario
    cy.wait(1000)
    getIframeBody().find("select").filter("[id='formRenderer:representante_tipo_voluntario']").select('01')  // 01 digitalizador aderido 03 Persona fisica
    cy.wait(1000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:representante_nif']").clear()
    cy.wait(5000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:representante_nif']").type(representante_nif, { force: true })
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:nombre_notario']").type(nombre_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:apellido1_notario']").type(apellido1_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:apellido2_notario']").type(apellido2_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:protocolo_notario']").type(protocolo_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").trigger('focus')
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").invoke('val', fecha_notario)
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").trigger('change')
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_nif']").clear()
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_nif']").type(entidad_autorizada_nif, {force: true})
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_razon_social']").clear()
    cy.wait(5000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_razon_social']")
      .type(entidad_autorizada_razon_social, {force: true})
    cy.wait(2000)


    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo']")
      .type(soli_documento_identificativo, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_razon_social']").type(soli_razon_social, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_telefono_movil']")
      .type(soli_telefono_movil, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_email']")
      .type(soli_email, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_persona_contacto']")
      .type(soli_persona_contacto, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='file']").parent().click({ force: true })
    getIframeBody().find("input[type='file']").parent().parent().click({ force: true })

    cy.wait(2000)

    getIframeBody().find("input[type='file']").parent().invoke('attr', 'style', 'overflow: visible').should('have.attr', 'style', 'overflow: visible')
    getIframeBody().find("input[type='file']").invoke('attr', 'style', 'opacity: 1').should('have.attr', 'style', 'opacity: 1')
    cy.wait(5000)


    getIframeBody().find("input[type='file']").selectFile({
      contents: 'cypress/downloads/Modelo Unificado v20220201_VersionWeb.pdf',
      fileName: 'Modelo Unificado v20220201_VersionWeb.pdf',
      mimeType: 'application/pdf'
    }, { force: true })

    cy.wait(20000)


    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    getIframeBody().find("select").filter("[id='formRenderer:autonomos_colaboradores_numero']").select(autonomos_colaboradores_numero.toString())
    .then(() => {
      if (autonomos_colaboradores_numero === 1) {
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_1_autonomo_nif']").type(AC_1_autonomo_nif, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_1_autonomo_nombre']").type(AC_1_autonomo_nombre, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_1_autonomo_apellidos']").type(AC_1_autonomo_apellidos, {force: true})
        cy.wait(1000)
      }

      if (autonomos_colaboradores_numero === 2) {
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_2_autonomo_nif']").type(AC_2_autonomo_nif, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_2_autonomo_nombre']").type(AC_2_autonomo_nombre, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_2_autonomo_apellidos']").type(AC_2_autonomo_apellidos, {force: true})
        cy.wait(1000)
      }
      if (autonomos_colaboradores_numero === 3) {
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_3_autonomo_nif']").type(AC_3_autonomo_nif, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_3_autonomo_nombre']").type(AC_3_autonomo_nombre, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_3_autonomo_apellidos']").type(AC_3_autonomo_apellidos, {force: true})
        cy.wait(1000)
      }
      if (autonomos_colaboradores_numero === 4) {
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_4_autonomo_nif']").type(AC_4_autonomo_nif, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_4_autonomo_nombre']").type(AC_4_autonomo_nombre, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter("[id='formRenderer:AC_4_autonomo_apellidos']").type(AC_4_autonomo_apellidos, {force: true})
        cy.wait(1000)
      }
    })
    cy.wait(2000)
    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)
    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion']").check()
    cy.wait(2000)
    getIframeBody().find("table").filter("[id='formRenderer:page_dc2_shifted']").click({ force: true})
    cy.wait(2000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_2']").check()
    cy.wait(2000)
    getIframeBody().find("table").filter("[id='formRenderer:page_de3_shifted']").click({ force: true})
    cy.wait(2000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_3']").check()
    cy.wait(2000)

    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:codigo_actividad_iae']").type(codigo_actividad_iae)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:minimis_recibida']").type(minimis_recibida)

    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_obligaciones']").check()
    cy.wait(2000)
    getIframeBody().find('.gf_render_next').click()
    cy.wait(2000)
    cy.get("button").eq(6).click()
    cy.wait(2000)
    cy.get("button").filter("[class='icon-page margin-left-10px margin-top-10px soloMovil']").first().click({ multiple: true })
    cy.wait(20000)

    cy.get("button").filter("[class='icon-page margin-left-10px margin-top-10px soloMovil']").first().click({ multiple: true })
    cy.wait(3000)
    cy.screenshot(soli_documento_identificativo)
    cy.get("button").filter("[name='_wizard_finish']").last().click({ force: true, multiple: true })
    cy.task('moveFile', soli_documento_identificativo)
  })
})
