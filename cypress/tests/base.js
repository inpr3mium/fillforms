const DNI_PRUEBAS = '53290137B'

// SOLICITANTE

const soli_documento_identificativo = '--soli_documento_identificativo'
const soli_razon_social = '--soli_razon_social'
const soli_telefono_movil = '--soli_telefono_movil'
const soli_email = '--soli_email'
const soli_persona_contacto = '--soli_persona_contacto'

// REPRESENTANTE

const representante_nif = '--representante_nif'
const nombre_notario = '--nombre_notario'
const apellido1_notario = '--apellido1_notario'
const apellido2_notario = '--apellido2_notario'
const protocolo_notario = '--protocolo_notario'
const fecha_notario = '--fecha_notario'

// AUTORIZADO

const entidad_autorizada_nif = '--entidad_autorizada_nif'
const entidad_autorizada_razon_social = '--entidad_autorizada_razon_social'
const entidad_autorizada_persona_contacto = '--entidad_autorizada_persona_contacto'
const entidad_autorizada_email = '--entidad_autorizada_email'
const entidad_autorizada_telefono_movil = '--entidad_autorizada_telefono_movil'


// COLABORADORES

const autonomos_colaboradores_numero = '--autonomos_colaboradores_numero'

const AC_1_autonomo_nif = '--AC_1_autonomo_nif'
const AC_1_autonomo_nombre = '--AC_1_autonomo_nombre'
const AC_1_autonomo_apellidos = '--AC_1_autonomo_apellidos'

const AC_2_autonomo_nif = '--AC_2_autonomo_nif'
const AC_2_autonomo_nombre = '--AC_2_autonomo_nombre'
const AC_2_autonomo_apellidos = '--AC_2_autonomo_apellidos'

const AC_3_autonomo_nif = '--AC_3_autonomo_nif'
const AC_3_autonomo_nombre = '--AC_3_autonomo_nombre'
const AC_3_autonomo_apellidos = '--AC_3_autonomo_apellidos'

const AC_4_autonomo_nif = '--AC_4_autonomo_nif'
const AC_4_autonomo_nombre = '--AC_4_autonomo_nombre'
const AC_4_autonomo_apellidos = '--AC_4_autonomo_apellidos'

const autonomosColaboradoresObj = {
  AC_1_autonomo_nif,
  AC_1_autonomo_nombre,
  AC_1_autonomo_apellidos,
  AC_2_autonomo_nif,
  AC_2_autonomo_nombre,
  AC_2_autonomo_apellidos,
  AC_3_autonomo_nif,
  AC_3_autonomo_nombre,
  AC_3_autonomo_apellidos,
  AC_4_autonomo_nif,
  AC_4_autonomo_nombre,
  AC_4_autonomo_apellidos
}
// DATOS FISCALES

const codigo_actividad_iae = '--codigo_actividad_iae'
const minimis_recibida = '--minimis_recibida'

const procedimiento = '--procedimiento'
const getIframeDocument = () => {
  return cy.get('iframe').its('0.contentDocument').should('exist')
}
const getIframeBody = () => {
  // get the document
  return getIframeDocument()
  // automatically retries until body is loaded
  .its('body').should('not.be.undefined')
  // wraps "body" DOM element to allow
  // chaining more Cypress commands, like ".find(...)"
  .then(cy.wrap)
}

describe('Base test', () => {
  it('should open form', () => {

    // ACCEDER AL FORMULARIO

    // cy.setCookie('JSESSIONID', '', {'domain':'sedepkd.red.gob.es'})
    // cy.setCookie('QueueITAccepted-SDFrts345E-V3_oficinaprof2', '', {'domain':'sedepkd.red.gob.es'})
    // cy.setCookie('QueueITAccepted-SDFrts345E-V3_oficinaprof', '', {'domain':'sedepkd.red.gob.es'})

    const formUrl = `${Cypress.env('URL')}proc=${procedimiento}`
    cy.readFile('session/cookies.json').then((cookies) => {
      cookies.forEach((c) => {
        cy.setCookie(c.name, c.value, c)
      })
    })
    cy.visit(formUrl)

    cy.pause()

    cy.getCookies()
    .then((cookies) => {
      cy.writeFile('session/cookies.json', cookies)
    })
    //cy.xpath('/html/body/div[5]/div[4]/div/div[1]/form/fieldset[1]/div/div[2]/button/span[2]').click()

    // STEP 1

    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo_ss']").type(DNI_PRUEBAS, {force: true})
    //cy.wait(200)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:nif_sgad']").type(DNI_PRUEBAS, {force: true})
    //cy.wait(200)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo_aeat']").type(DNI_PRUEBAS, {force: true})
    //cy.wait(200)


    getIframeBody().find("select").filter("[id='formRenderer:soli_empresa_autoempleo']").select('autoempleo') // Representante voluntario *
    cy.wait(5000)
    getIframeBody().find("select").filter("[id='formRenderer:soli_provincia']").select('--soli_provincia') // Provincia solicitante  *
    cy.wait(5000)
    getIframeBody().find("input[type='radio']").filter("[id='formRenderer:soli_empresas_vinculadas:1']").check() // Check checkbox element *
    cy.wait(1000)

    getIframeBody().find("select").filter("[id='formRenderer:representante_tipo']").select('02') // Representante voluntario *
    cy.wait(3000)
    getIframeBody().find("select").filter("[id='formRenderer:representante_tipo_voluntario']").select('01')  // 01 digitalizador aderido 03 Persona fisica *
    cy.wait(3000)

    getIframeBody().find("input[type='file']").parent().click({ force: true })
    getIframeBody().find("input[type='file']").parent().parent().click({ force: true })

    cy.wait(2000)

    getIframeBody().find("input[type='file']").parent().invoke('attr', 'style', 'overflow: visible').should('have.attr', 'style', 'overflow: visible')
    getIframeBody().find("input[type='file']").invoke('attr', 'style', 'opacity: 1').should('have.attr', 'style', 'opacity: 1')
    cy.wait(3000)

    getIframeBody().find("input[type='file']").trigger('focus')
    cy.wait(3000)

    getIframeBody().find("input[type='file']").selectFile({
      contents: 'cypress/downloads/' + '--soli_documento_identificativo' + '.pdf',
      fileName: '--soli_documento_identificativo' + '.pdf',
      mimeType: 'application/pdf'
    }, { force: true })

    cy.wait(30000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:representante_nif']").clear()
    //cy.wait(5000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:representante_nif']").type(representante_nif, { force: true })
    //cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:nombre_notario']").type(nombre_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:apellido1_notario']").type(apellido1_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:apellido2_notario']").type(apellido2_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:protocolo_notario']").type(protocolo_notario, { force: true })
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").trigger('focus')
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").invoke('val', fecha_notario)
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:fecha_notario']").trigger('change')
    cy.wait(2000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_nif']").clear()
    //cy.wait(2000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_nif']").type(entidad_autorizada_nif, {force: true})
    //cy.wait(2000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_razon_social']").clear()
    //cy.wait(5000)
    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_razon_social']")
    //  .type(entidad_autorizada_razon_social, {force: true})
    //cy.wait(2000)
//

    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_documento_identificativo']")
    //  .type(soli_documento_identificativo, {force: true})
    //cy.wait(2000)

    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_razon_social']").type(soli_razon_social, {force: true})
    //cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_telefono_movil']").clear()
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_telefono_movil']") // *
      .type(soli_telefono_movil, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_email']").clear()
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_email']") // *
      .type(soli_email, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_persona_contacto']").clear()
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:soli_persona_contacto']") // *
      .type(soli_persona_contacto, {force: true})
    cy.wait(2000)


    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_telefono_movil']").clear()
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_telefono_movil']") // *
      .type(entidad_autorizada_telefono_movil, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_email']").clear()
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_email']") // *
      .type(entidad_autorizada_email, {force: true})
    cy.wait(2000)

    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_persona_contacto']").clear()
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:entidad_autorizada_persona_contacto']") // *
        .type(entidad_autorizada_persona_contacto, {force: true})
    cy.wait(2000)

    //getIframeBody().find("[id='formRenderer:j_id56']").focus()
    //cy.wait(2000)
    cy.pause()
    getIframeBody().find("[id='formRenderer:j_id56']").click()

    // STEP 2 COLABORADORES
    cy.pause()
    getIframeBody().find("select").filter("[id='formRenderer:autonomos_colaboradores_numero']").select(autonomos_colaboradores_numero.toString())
    .then(() => {
      function fillAutonomoForm (autonomoNum) {
        const nif = autonomosColaboradoresObj[`AC_${autonomoNum}_autonomo_nif`]
        const nombre = autonomosColaboradoresObj[`AC_${autonomoNum}_autonomo_nombre`]
        const apellidos = autonomosColaboradoresObj[`AC_${autonomoNum}_autonomo_apellidos`]
        getIframeBody().find("input[type='text']").filter(`[id='formRenderer:AC_${autonomoNum}_autonomo_nif']`).type(nif, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter(`[id='formRenderer:AC_${autonomoNum}_autonomo_nombre']`).type(nombre, {force: true})
        cy.wait(1000)
        getIframeBody().find("input[type='text']").filter(`[id='formRenderer:AC_${autonomoNum}_autonomo_apellidos']`).type(apellidos, {force: true})
        cy.wait(1000)
      }
      for (let i = 1; i <= parseInt(autonomos_colaboradores_numero); i++) {
        fillAutonomoForm(i)
        if (i == 1) {getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_autonomos_colaoradores']").check()
        cy.wait(2000)}
      }
    })
    cy.wait(2000)
    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    // STEP 3 CHECK

    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_autorizaciones_ss']").check()
    cy.wait(2000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_autorizaciones_aeat']").check()
    cy.wait(2000)

    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    // STEP 4 CHECKS



    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion']").check()
    cy.wait(2000)
    getIframeBody().find("table").filter("[id='formRenderer:page_dc2_shifted']").click({ force: true})
    cy.wait(2000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_2']").check()
    cy.wait(2000)
    getIframeBody().find("table").filter("[id='formRenderer:page_de3_shifted']").click({ force: true})
    cy.wait(2000)
    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_3']").check()
    cy.wait(2000)

    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    //getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion']").check()
    //cy.wait(2000)
    //getIframeBody().find("table").filter("[id='formRenderer:page_dc2_shifted']").click({ force: true})
    //cy.wait(2000)
    //getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_2']").check()
    //cy.wait(2000)
    //getIframeBody().find("table").filter("[id='formRenderer:page_de3_shifted']").click({ force: true})
    //cy.wait(2000)
    //getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_declaracion_3']").check()
    //cy.wait(2000)

    //getIframeBody().find('.gf_render_next').click()
    //cy.wait(3000)

    // STEP 5 INFORMACION FISCAL

    // 65450
    //<option value="7191">6545 - FARMACIAS</option> // data-option-array-index="522" style="">652.1 - FARMACIAS -- id="formRenderer_codigo_actividad_iae_chosen"

    //getIframeBody().find("select").filter("[id='formRenderer_codigo_actividad_iae']").select('1835', { force: true }).invoke('val').should('eq', '1835') // Representante voluntario *
    cy.pause()

    //getIframeBody().find("input[type='text']").filter("[id='formRenderer:codigo_actividad_iae']").type(codigo_actividad_iae)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:minimis_recibida']").clear()
    cy.wait(2000)
    getIframeBody().find("input[type='text']").filter("[id='formRenderer:minimis_recibida']").type(minimis_recibida)
    cy.wait(3000)

    getIframeBody().find('.gf_render_next').click()
    cy.wait(3000)

    // STEP 6 CHECK

    getIframeBody().find("input[type='checkbox']").filter("[id='formRenderer:check_obligaciones']").check()
    cy.wait(2000)
    getIframeBody().find('.gf_render_next').click()

    // STEP 7.0
    //  Firma
    cy.wait(10000)
    //cy.get("button").eq(6).click()
    cy.get("button").contains("Firmar").click({ multiple: true })
    cy.wait(20000)
    //  Presentacion icon-asistente
    cy.get("button").contains("Registrar").click({ multiple: true })
    cy.wait(20000)
    //cy.get("button").filter("[class='icon-page margin-left-10px margin-top-10px soloMovil']").first().click({ multiple: true })
    //cy.wait(20000)
    // STEP 8

    //cy.get("button").filter("[class='icon-page margin-left-10px margin-top-10px soloMovil']").first().click({ multiple: true })
    //cy.wait(3000)

    // STEP 9 CAPTURA, MOVER ARCHIVO Y FINALIZAR
    cy.screenshot(soli_documento_identificativo)
    cy.get("button").contains("Finalizar").click({ multiple: true })
    cy.wait(3000)
    //cy.get("button").filter("[name='_wizard_finish']").last().click({ force: true, multiple: true })
    cy.task('moveFile', soli_documento_identificativo)
  })
})
